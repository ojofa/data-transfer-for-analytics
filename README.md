### Student Data Transfer for Data Analytics

#### Table of Contents

1. [Scenario](#scenario)
2. [Requirements](#requirements)
    1. [Functional requirements](#functional-requirements)
    2. [Non-functional requirements](#non-functional-requirements)
    3. [System constraints](#system-constraints)
3. [Deployment](#deployment)
4. [Best Practices](#best-practices)
5. [Resources](#resources)

## Scenario (Fictitious)
- In order to gain additional grants and funding to boost overall student success and engagement,
  Miami University is partnering with multiple third-party Data Analytics companies to analyze
  the data for active undergraduate and graduate students. The goal is to gain key insights from
  the analyzed data to make predictions, arrive at informed decisions, revise business models, and optimize
  the performance capabilities of colleges across all Miami Campuses. In total, there are 4 Data
  analytics companies, each interested in data from specific colleges as indicated in the table below.

    * Data Analytics Companies
        1. BusinessDataSystems
        2. EngineeringAnalytics
        3. DataInsightsGlobal
        4. AnalyticalLabs
    * Colleges of interest
        1. 	Farmer School of Business (BU)
        2.	College of Engineering & Computing (AP)
        3.	College of Arts and Science (AS)
        4.	College of Creative Arts (FA)
        5.	College of Liberal Arts & Applied Sciences (RC)
    * Table showing Company with interested data
  ---
  | Company               | College | Data Upload / Format      |
  |-----------------------|---------------------|---------------------------|
  | BusinessDataSystems   | Farmer School of Business (BU) | API (UG and GR)           |
  | EngineeringAnalytics  | College of Engineering & computing | API, File (CSV) to SFTP Server |
  | DataInsightsGlobal | College of Arts and Science (AS), College of Creative Arts (FA) | File (CSV) and File (JSON) to AWS S3 Bucket|
  | AnalyticalLabs  | College of Liberal Arts & Applied Sciences (RC) | Database – AWS DynamoDB|
  ---
    * Notes
        * Though the data that need to be sent to every company vary, all companies
          need the required data to identify the student
            1. Name
            2. Email
            3. UniqueId
            4. Level Code (Undergraduate - UG, Graduate - GR)
        * Student data
            1. Some students have only one major in a specific college.
            2. It is common to have students belonging to more than one college. For example,
               the same undergraduate student may have a double major with one major such as
               Mechanical Engineering in the College of Engineering and the other major such as
               Arts Management in the College of Arts.
            3. Some students are in the accelerated program of study, implying that they
               have an active undergraduate record and an active graduate record

## Requirements
### Functional requirements - Features
These describe the features of the system, also known as the functions of the system. 
These requirements help to determine the expected behavior of the system. 
Functional requirements are detailed in the system design. 
are
1. An application (job/process) that performs a weekly upload of student data from the various colleges to the data analytics companies specified above.
   The data format for the upload should match with the formats specified in the table above.
2. An API to retrieve all participating colleges at Miami University (specified above).
3. An API that retrieves records for active undergraduate and graduate students in
   each college specified above. In this context, an active student is one that is currently registered
   for classes in the current term/semester.
4. An application that adapts to any changes in the data destination company for one or more colleges
5. An application that allows for data upload in different formats and to multiple outputs / destinations
   such as SFTP server, AWS S3 Bucket, REST API, File output (CSV, JSON), etc. 
6. At any time, each company can subscribe or unsubscribe itself for any particular college data.
7. Each college can decide anytime that their data needs to be fetched from a
   particular source (Miami University Banner DB, API, File) and sent to a
   desired destination (Data Analytics Company)


### Non-functional requirements
These describe the qualities a system must have in order for it to perform as expected. 
Non-functional requirements are detailed in the system architecture. 
requirements. 
1. Performance
2. Scalability
3. Availability
4. Reliability
5. Security

### System constraints 
`TODO`

## Deployment
* Deployment
    * AWS Infrastructure
* Version Control - Any of the following:
    * GitHub, GitLab

## Best Practices
1. [Design principles](docs/application_design_and_development.md)
2. Design Patterns
4. Avoid fetching several thousands of student data into memory (RAM). This is not ideal.
    1. Considerations
        1. FlyWeight Design Pattern?
        2. Other Data Access Patterns. E.g Repository pattern (Pros and Cons?)

## Resources & References
1. [Data Access Patterns](https://medium.com/mastering-software-engineering/data-access-patterns-the-features-of-the-main-data-access-patterns-applied-in-software-industry-6eff86906b4e)
2. [Code Complexity – CRAP score](https://blog.ndepend.com/crap-metric-thing-tells-risk-code/)
3. Tools
    1. Mind Mapping tools
        1. XMind
    2. Collaboration tools
        1. Miro
        2. Google JamBoard
    3. APIs
        1. StopLight - Develop APIs collaboratively with an API first design

