###  Application Design and Development
Implementing a good Object-Oriented Design often leads to applications that are:
1.	Flexible
2.	Easy to change
3.	Maintainable
4.	Reusable
      
#### Object-Oriented Programming – Core Concepts
1.	Inheritance
2.	Encapsulation
3.	Polymorphism
4.	Abstraction

#### Fundamental / Core Design Principles
1. **Encapsulate what varies**
   - If there is a part of our application that keeps changing, for example, with every new requirement, then encapsulate that part away from the rest of the design
2. **Favor composition over inheritance**
   - This principle warns against an over use of inheritance and suggests composition as a powerful alternative for extending behavior in our design. Too much use of inheritance often leads to an inflexible design
3. **Program to Interfaces, not implementations** 
   - Keep the design high level and referring, where possible, to abstractions or interfaces and not concrete implementations. This makes it easier to swap out implementations.
4. **Loose coupling principle** 
   - Strive for loosely coupled designs between objects that interact
   - Where applicable, the Observer design pattern is great for a loosely-coupled design

#### SOLID Principles
- S – Single Responsibility Principle 
  - A class should have only one reason to change
- O – Open Closed principle 
  - A class should be open for extension, but closed for modification
- L – Liskov Substitution principle 
  - Subtypes must be substitutable for their base types
- I – Interface Segregation principle 
  - Keep interfaces small and cohesive
- D – Dependency Inversion principle 
  - High-level modules should not depend on low-level modules. Both high-level modules and low-level modules should depend on abstractions, not concretions. 

#### Symptoms of inefficient design
1. Rigidity 
   - Hard to change due to dependencies 
   - Changes in one part of the application leads to cascading changes in other parts of the application
2. Fragility 
   - The application is fragile if it breaks easily
   - As one makes changes in one part of the application, unexpected bugs occur in unrelated parts of the application.
   - Technical defects occur too often during feature enhancements or while working on fixing other bugs
   - No error handling or improper handling of exceptions
3. Immobility 
   - The application and its components are hard to reuse. Inability to take the current design and reuse it in places it wasn’t designed for.
4. Not Resilient to change 
   - Difficult to make changes to the application. Whether by changing business requirements / customer needs, or other changes, it’s hard to make changes to this application.
