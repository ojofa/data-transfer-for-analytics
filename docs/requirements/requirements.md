## Requirements

#### Table of Contents

1. [Business Requirements](#business-requirements)
2. [Application Requirements](#application-requirements)
3. [General Requirements](#general-requirements)
4. [Technical Requirements](#technical-requirements)
5. [Functional Requirements](#functional-requirements)
6. [Non-Functional Requirements](#non-functional-requirements)
7. [Collaboration and Working Requirements](#collaboration-and-working-requirements)

### Business Requirements
1. An application (job/process) that performs a weekly upload of student data from the various colleges to the data analytics companies specified above.
   The data format for the upload should match with the formats specified in the table above.
2. An API to retrieve all participating colleges at Miami University (specified above).
    1. Question: Laravel for API versus vanilla PHP? Pros and Cons?
3. An API that retrieves records for active undergraduate and graduate students in
   each college specified above. In this context, an active student is one that is currently registered
   for classes in the current term/semester.
4. An application that adapts to changes in the data destination company for one or more colleges
5. An application that allows for data upload in different formats and to multiple outputs / destinations
   such as SFTP server, AWS S3 Bucket, REST API, File output (CSV, JSON), etc.
6. At any time, each company can subscribe or unsubscribe itself for any particular college data.
    1. Consideration: Observer Design Pattern
7. Each college can decide anytime that their data needs to be fetched from a
   particular source (Miami University Banner DB, API, File) and sent to a
   desired destination (Data Analytics Company)

### Application Requirements

* #### General Requirements
    1. Create a job that performs the upload for the various colleges
    2. Create an API to retrieve:
        1. all the colleges for the university
        2. active undergraduate and graduate students based on college code
    3. Filters for the API may include:
        1. Level Code: Undergraduate (UG), Graduate (GR)
        2. Major - Get students from a particular college based on major
        3. Nice to have - Filter on Minor
    4. The processing of one particular college data (fetching and uploading) shouldn’t affect the processing for another college data.
    5. The source for the college data could be a database, file (CSV, JSON, Yaml), REST API, etc.
    6. The destination for uploading/updating/storing the college data could be a database, REST API, file, etc.
    7. Destination data
        1. Each college may have a similar or different set of data to send to the destination.
        2. The destination data may overlap for multiple colleges
        3. Some colleges may not send/upload the same data as other colleges
    8. Upon completion of processing (fetching and sending) data for a particular college, based on a configured setting for the particular college, send, or do not send, a success notification.
    9. A notification could be either email, slack message, or text message (critical system alerts).


* #### Technical Requirements
    1. Persistence
        1. Ability to easily swap the persistence layer of the application
            1. The application work fine while adapting to any changes in the persistence layer. Ensure abstraction
               and test the application to fetch data from:
                - Database
                - API
                - File (CSV, Json, Yaml, etc.)
            2. Consideration: Use the Repository pattern
                1. Be mindful that some classes don't need to understand what the persistence layer is
                   and/or how to fetch the information. Classes that rely on the data from the persistence
                   layer could use (dependency injection) the data repository
    2. Data Retrieval - Output
        1. For data retrieval, ability to easily change or use multiple / different formats of data.
           Examples: Json formatting as an output for additional processing, HTML formatting for the presentation
           layer or the user interface.
            1. It's important to keep the SRP (Single Responsibility Principle) in mind.
               It's not the responsibility of every class to determine how to format or print the data
    3. Queues
        1. Easily swap out the queueing system. E.g the application should accomodate switching
           between Laravel queues or AWS queues, etc.
        2. Ensure retry operations for failed queued jobs
    4. API calls
        1. Leverage one API client to make calls to multiple APIs. Example, the same apiClient may
           work with uploading data to different APIs for different colleges. The apiClient should not
           be tightly coupled to any particular Data Analytics / College API.
        2. Updates to existing records - PATCH vs PUT API method call
            1. Only update the data that needs to be changed/updated at the destination (API, file, etc.).
               To avoid unintended overwrites of existing fields, use PATCH instead of PUT. PUT updates the
               entire record. If the record isn't found, PUT method call creates a new record.
    5. Testing
        1. To ensure quality during the application development lifecycle to the final product,
           TDD (Test-Driven Development) should be followed to promote high test coverage, minimize bugs,
           and facilitate documentation
            1. Feature tests may not rely on real external services or actual
               implementations of the Depended-On Components (DOC). The System Under Test (SUT)
               may utilize Test Doubles to replace actual implementations. Example:
               Feature tests may not depend on actual database connections.
                1. Does PHPUnit in-memory database apply in this case?
                2. For Feature tests, when is it a good practice to:
                    1. Setup PHPUnit in-memory database?
                    2. Not set up PHPUnit in-memory database?
    6. Notifications
    7. Error / Exception Handling
    8. Logging
    9. If using MVC - Model View Controller pattern
        1. Controllers should not contain the domain business logic
        2. Business classes utilized in the controllers should not be aware of the HTTP interface

#### Functional Requirements
- `TODO`

#### Non-Functional Requirements
- `TODO`

#### Collaboration and Working Requirements
1. Availability of the Product Owner, and where necessary, Subject-Matter Experts (SME).
2. Ability of the Product Owner to order and prioritize the product backlog from items
   that produce most desired outcome to items with the least value.
3. Quick feedback loop with the Product Owner, SME, and/or involved stakeholders.